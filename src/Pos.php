<?php
declare(strict_types=1);

final class Pos
{
    private $skuList = [];
    private $productCatalog;

    public function __construct(ProductCatalog $productCatalog)
    {
        $this->productCatalog = $productCatalog;
    }

    public function getCartTotalPrice()
    {
        $totalPrice = 0;
        foreach ($this->skuList as $sku) {
            $totalPrice += $this->productCatalog->getProductPrice($sku);
        }
        return $totalPrice;
    }

    public function addProduct($sku)
    {
        $this->skuList[] = $sku;

    }

    public function getProductList()
    {
        return $this->skuList;
    }
}
