<?php
declare(strict_types=1);

abstract class ProductCatalog
{

    abstract public function getProductPrice($sku);
}
