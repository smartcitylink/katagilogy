<?php
declare(strict_types=1);

require __DIR__ . "/../src/ProductCatalog.php";

final class InMemoryProductCatalog extends ProductCatalog
{
    private $catalog = [];

    public function __construct($catalog)
    {
        $this->catalog = $catalog;
    }

    public function getProductPrice($sku)
    {
        return $this->catalog[$sku];
    }
}
