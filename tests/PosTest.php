<?php
declare(strict_types=1);

require __DIR__ . "/../src/Pos.php";
require __DIR__ . "/InMemoryProductCatalog.php";

use PHPUnit\Framework\TestCase;

final class PosTest extends TestCase
{
    private $sku = 2;
    private $sku2 = 3;
    private $price = 1.95;
    private $price2 = 3.87;

    public function testEmptyCart()
    {
        $productCatalog = new InMemoryProductCatalog([]);
        $pos = new Pos($productCatalog);

        $productList = $pos->getProductList();
        $this->assertCount(0, $productList);

        $totalPrice = $pos->getCartTotalPrice();
        $this->assertEquals(0, $totalPrice);
    }

    public function testAddProduct()
    {
        $catalog = [$this->sku => $this->price];
        $productCatalog = new InMemoryProductCatalog($catalog);

        $pos = new Pos($productCatalog);
        $pos->addProduct($this->sku);
        $productList = $pos->getProductList();
        $this->assertCount(1, $productList);

        $totalPrice = $pos->getCartTotalPrice();
        $this->assertEquals($this->price, $totalPrice);
    }

    public function testGetProductInfo()
    {
        $catalog = [$this->sku => $this->price, $this->sku2 => $this->price2];
        $productCatalog = new InMemoryProductCatalog($catalog);
        $this->assertEquals($this->price, $productCatalog->getProductPrice($this->sku));
        $this->assertEquals($this->price2, $productCatalog->getProductPrice($this->sku2));
    }

    public function testAddTwoProducts()
    {
        $catalog = [$this->sku => $this->price, $this->sku2 => $this->price2];
        $productCatalog = new InMemoryProductCatalog($catalog);

        $pos = new Pos($productCatalog);
        $pos->addProduct($this->sku);
        $pos->addProduct($this->sku2);
        $productList = $pos->getProductList();
        $this->assertCount(2, $productList);

        $totalPrice = $pos->getCartTotalPrice();
        $this->assertEquals($this->price + $this->price2, $totalPrice);
    }
}
